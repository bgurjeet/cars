<?php 
function main_menu() {
    register_nav_menu('primary','top_navigation');
}

add_action('init','main_menu');


// fetching style 
function stylefetch() {
    wp_enqueue_style( 'main',get_template_directory_uri().'/style.css');
    
}
add_action( 'wp_enqueue_scripts', 'stylefetch');


function cars_post() {
    register_post_type( 'cars', array(
        'labels' => array(
            'name' => __('cars'),
            'singular_name' => __('cars'),
            'add_new_item' => __('Add Item'),
            'all_item' => __('All Item'),
            'new_item' => __('New Item'),
            'edit_item' => __('Edit Item'),
            'view_item' => __('View Item'),
            'featured_image' => __('Feature Image'),
            'set_featured_image' => __('set_feature_image'),
            'view_featured_image' => __('view_featured_image'),
            'remove_featured_image' => __('remove_featured_image')
        ),
        'public' => true,
        'has_archive' => true,
        'rewrite' => array('slug' => 'cars'),
        'supports' => array('title', 'thumbnail','revision','excerpt','custom-field','author','comments'),
        'taxonomies'  => array( 'category','post_tag'),
    )
);
}

add_action( 'init', 'cars_post');


function add_feature_image() {
    add_theme_support('post-thumbnails');
}

add_action('after_setup_theme','add_feature_image');

function custom_excerpt_length( $length ) {
    return 50;
}
add_filter( 'excerpt_length', 'custom_excerpt_length');

?>