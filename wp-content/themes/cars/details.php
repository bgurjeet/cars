<?php 
// Template Name: data
get_header(); ?>
<main>
    <?php 
        get_template_part( 'template-part/title');
        get_template_part( 'template-part/excerpts');
        get_template_part( 'template-part/lists'); 
    ?>
</main>
<?php 
get_footer();
?>