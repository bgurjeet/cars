<?php 
$lists = new WP_Query(array(
    'post_type' => 'cars',
    'cat' => '2'
)); ?>
<ul class='title'>
<?php 
if($lists->have_posts()):while($lists->have_posts()):$lists->the_post(); ?>
    <li>
    <figure><?php echo get_the_post_thumbnail(); ?></figure>
    <span><?php echo get_the_date(); ?></span>
    <h3><?php echo get_the_title(); ?></h3>
    <p><?php echo get_the_excerpt(); ?></p>
    <a href="<?php echo get_the_permalink();?>" title="<?php echo get_the_title(); ?>">Read more</a>
    </li>
<?php 
endwhile; else:
    echo "no content";
endif;
?>
</ul>