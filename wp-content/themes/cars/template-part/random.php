<?php 
$data = new WP_Query(array(
    'post_type' => 'cars',
    'posts_per_page' => 3,
    'orderby' => 'rand'
)); ?>
<ul>
<?php if($data->have_posts()):while($data->have_posts()):$data->the_post(); ?>
    <li>
        <h2><?php echo get_the_title(); ?></h2>
        <figure><?php echo get_the_post_thumbnail(); ?></figure>
    </li>
<?php 
endwhile;else:
    echo 'no posts';
endif;
?>
</ul>