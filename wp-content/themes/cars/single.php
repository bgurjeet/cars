<?php get_header();  ?>
<main>
<div class="content">
<?php 
if(have_posts()):while(have_posts()):the_post(); 
$val = get_field('cars_detail');
?>  
    <figure><?php echo get_the_post_thumbnail(); ?></figure>
    <p><?php echo $val;?></p>
<?php 
endwhile;else:
endif;
?>
</div>
<div class='lists'>
    <?php get_template_part('template-part/random'); ?>
</div>
</main>
<?php 
get_footer();
?>